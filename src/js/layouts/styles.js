export default {
  rootStyle: {
    height: '100%',
    position: 'relative',
    fontFamily: '"Helvetica Neue", sans-serif'
  },
  Wrapper: {
    textAlign: 'center',
    fontSize: 25,
    color: '616161'
  }
};
