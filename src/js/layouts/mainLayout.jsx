import React, { Component } from 'react';

import Header from '../containers/header';
import Styles from './styles';

class MainLayout extends Component {
  render() {
    return (
      <div style={Styles.rootStyle}>
        <div style={Styles.Wrapper}>
          <span> Hello, Telio User </span>
          <Header />
        </div>
      </div>
    );
  }
}

export default MainLayout;
