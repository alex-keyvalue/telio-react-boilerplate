import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import MainLayout from './mainLayout';

function mapStateToProps() {
  return {
    //
  };
}

export default withRouter(connect(mapStateToProps, null)(MainLayout));
