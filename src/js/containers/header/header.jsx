import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Styles from './styles';

class Header extends Component {
  handleClick = () => {
    const { onClickGreetingsButton } = this.props;
    onClickGreetingsButton('Telio user');
  }

  render() {
    const { message } = this.props;
    return (
      <div style={Styles.rootStyle}>
        <span>
          {message}
        </span>
        <button type="button" onClick={this.handleClick}> Get greetings </button>
      </div>
    );
  }
}

Header.propTypes = {
  onClickGreetingsButton: PropTypes.func.isRequired,
  message: PropTypes.string
};

Header.defaultProps = {
  message: ''
};

export default Header;
