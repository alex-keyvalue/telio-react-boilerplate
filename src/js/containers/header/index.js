import { connect } from 'react-redux';
import Immutable from 'seamless-immutable';

import Header from './header';

export * from './store/sagas';

const mapStateToProps = state => ({
  message: Immutable.asMutable(state.header, { deep: true }).message
});

const mapDispatchToProps = dispatch => ({
  onClickGreetingsButton: (userName) => {
    dispatch({ type: 'GREETINGS:GET:INIT', userName });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
