import { call } from 'redux-saga/effects';
import apiCall from '../../../../sagas/api';

export function* greetTelioUser(action) { // eslint-disable-line
  const apiArgs = {
    API_CALL: {
      method: 'GET',
      url: ''
    },
    responseKey: 'greetings',
    TYPES: ['GET:GREETINGS:SUCCESS', 'GET:GREETINGS:FAILED']
  };
  yield call(apiCall, apiArgs);
}
