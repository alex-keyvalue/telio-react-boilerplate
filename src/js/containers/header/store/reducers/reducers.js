import Immutable from 'seamless-immutable';

const defaultState = Immutable({
  dummyMessage: 'Have a nice day'
});

const HeaderReducer = (state = defaultState, action) => {
  switch (action.type) {
    /* eslint-disable no-case-declarations */
    case 'GREETINGS:GET:INIT':
      return state.merge({ message: state.dummyMessage });
    case 'GET:GREETINGS:SUCCESS':
      return state.merge({ greetings: action.greetings });
    default:
      return state;
  }
};

export default HeaderReducer;
