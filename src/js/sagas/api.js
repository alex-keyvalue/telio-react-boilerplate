import axios from 'axios';
import { put, call } from 'redux-saga/effects';
import _ from 'lodash';

export default function* apiCall(payload) { // eslint-disable-line
  const { API_CALL, TYPES: [successType, failureType], responseKey } = payload;
  try {
    // Make API call
    const apiResponse = yield call(axios, API_CALL);
    if (apiResponse.data) { // If API call is a success
      if (successType) yield put({ type: successType, [responseKey]: apiResponse.data });
    }
  } catch (err) { // If API response is a failure
    if (failureType) {
      yield put({ type: failureType, payload: _.get(err.response, 'data') });
    }
  }
}
