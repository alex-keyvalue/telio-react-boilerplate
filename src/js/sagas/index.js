import { takeEvery } from 'redux-saga';

import { greetTelioUser } from '../containers/header/store/sagas';

const sagas = [
  // All root sagas needs to go in here
  [takeEvery, 'GREETINGS:GET:INIT', greetTelioUser]
];

function* rootSaga() {
  yield [
    sagas.map(saga => function* () {
      yield saga[0](saga[1], saga[2]);
    }).map(saga => saga.call())
  ];
}

export default rootSaga;
