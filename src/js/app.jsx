import React from 'react';
import 'babel-polyfill';
import registry from 'app-registry';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter as Router } from 'react-router-dom';

import MainLayout from './layouts';
import storage from './services/storage';
import logger from './services/logger';
import { store } from './store';

registry.register('storage', storage);
registry.register('logger', logger);
registry.register('store', store);

/* eslint-disable no-undef */
if (typeof appConfig !== 'undefined') {
  const config = appConfig || {};
  registry.register('config', config);
  if (config.logger && config.logger.level) {
    logger.setLevel(config.logger.level);
  }
} else {
  registry.get('logger').warning('WARNING: the config is not defined');
}
/* eslint-enable no-undef */

store.dispatch({ type: 'APP:INIT' });

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <MainLayout />
    </Router>
  </Provider>,
  document.getElementById('app')
);
